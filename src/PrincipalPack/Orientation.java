package PrincipalPack;

public class Orientation {

    //dynamic values
    char verticalHorizon;
    int valueScalar;

    //finally values
    int finalValueX;
    int finalValueY;

    public int getFinalValueY() {
        return finalValueY;
    }

    public void setFinalValueY(int finalValueY) {
        this.finalValueY = finalValueY;
    }

    public int getFinalValueX() {
        return finalValueX;
    }

    public void setFinalValueX(int finalValueX) {
        this.finalValueX = finalValueX;
    }

    //default constructor
    public Orientation() {
        verticalHorizon = '\u0000';
        valueScalar = 0;
    }

    public Orientation(char initVertHorizon, int initValueScalar) {
        verticalHorizon = initVertHorizon;
        valueScalar = initValueScalar;
    }

    public char getVerticalHorizon() {
        return verticalHorizon;
    }

    public int getValueScalar() {
        return valueScalar;
    }

    public void setVerticalHorizon(char vertHorizon) {
        this.verticalHorizon = vertHorizon;
    }

    public void setValueScalar(int valueScalar) {
        this.valueScalar = valueScalar;
    }
}


