package PrincipalPack;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PrincipalClass {

    //absolute path for all platform
    static Path pathFileCarte = Paths.get("carte.txt");
    static String filePathCarte = String.valueOf(pathFileCarte.toAbsolutePath());

    static Path pathFileRoader1 = Paths.get("roader1.txt");
    static Path pathFileRoader = Paths.get("roader.txt");
    static String filePathRoader = String.valueOf(pathFileRoader.toAbsolutePath());

    static int lineNumber;
    public char ARRAY[][][];
    public HashMap<String, List<Integer>> dimentionsArrayByFiles; //= new HashMap<String, List<Integer>>();

    public static void main (String args[]){
        try {
            char ARRAY[][][];
            HashMap<String, List<Integer>> dimentionsArrayByFiles;

            //calling plusieurs methodes
            PrincipalClass priCla = new PrincipalClass();

            //instancement of inner Class Reading
            PrincipalClass.Reading reaPriCla = priCla.new Reading();

            //function for checking max dim et currency dimention of multidimentional ARRAY
            dimentionsArrayByFiles=reaPriCla.subDimentionInput(filePathCarte);
            //getValues bye Keys
            System.out.println("dimentions of array max"
                    + dimentionsArrayByFiles.get(filePathCarte).get(0)
                    + ":" + dimentionsArrayByFiles.get(filePathCarte).get(1)
                    + ":" +  dimentionsArrayByFiles.get(filePathCarte).get(2)
                    + ":" +  dimentionsArrayByFiles.get(filePathCarte).get(3));

            ARRAY = new char [dimentionsArrayByFiles.get(filePathCarte).get(0)]  //
                    [dimentionsArrayByFiles.get(filePathCarte).get(1)]
                    [dimentionsArrayByFiles.get(filePathCarte).get(3)];

            System.out.println(ARRAY);

            //subroutine of input file carte
            reaPriCla.subReadingInput(dimentionsArrayByFiles, ARRAY, filePathCarte);

            System.out.println(ARRAY);

            //function for checking max dim et currency dimention of multidimentional ARRAY
            dimentionsArrayByFiles=reaPriCla.subDimentionInput(filePathRoader);

            //subroutine of input file roader
            reaPriCla.subReadingInput(dimentionsArrayByFiles, ARRAY, filePathRoader);

            System.out.println(ARRAY[0][0][1]+"_starting point in main program:_"+ARRAY[0][1][1]);

            //instancement of external Class
            CoordonneesInput coordonneesInput = new CoordonneesInput(ARRAY[0][0][1],ARRAY[0][1][1]);

            //instancement of external Class
            Orientation orientation = new Orientation();

            orientation=coordonneesInput.lectureCalculeNumerique(ARRAY);

            CoordonneesO coordonneesOutput=new CoordonneesOutput();

            System.out.println(coordonneesOutput.outputCoordonnes(orientation));

        }catch(Exception e){
            System.out.println(e);
        }
    }

    //inner Class 1
    public class Reading {

        public HashMap<String, List<Integer>> subDimentionInput(String filePath) {
            try {
                //instanciation des objets IO de Java
                System.out.println(filePath);

                /*not good for impossibility to move the cursor every where in the file
                FileReader fileReaderBuffer = new FileReader(filePath);
                BufferedReader bufferedReader = new BufferedReader(fileReaderBuffer);
                incompatible objects: if you want use LineNumberIndependently , you must created another object(reference)
                for LineNumberLine*/
                FileReader fileReaderNumber = new FileReader(filePath);
                LineNumberReader lineNumberReader = new LineNumberReader(fileReaderNumber);
                //
                RandomAccessFile randomAccessFile = new RandomAccessFile(filePath, "rw");

                long positionFile = randomAccessFile.getFilePointer();

                int i = -1;
                int lineCount = 0;
                int sizeLine = 0;
                int threeDimention =0;
                String lineContentRandom = "";
                //iteration for checking dimentions of multidimentional array
                do {
                    i = i + 1;

                    //position for pointer
                    positionFile = randomAccessFile.getFilePointer();
                    //position for lenght of line (in ce cas 20)
                    //reading content line
                    lineContentRandom = randomAccessFile.readLine();

                    //needed only for have -1 at the last line;
                    lineNumberReader.readLine();
                    lineCount = lineNumberReader.read();

                    //count line number

                    lineNumber = lineNumberReader.getLineNumber();

                    if (lineCount == -1) {
                        //la lenght est constant
                        sizeLine = lineContentRandom.getBytes().length;
                    }

                    System.out.println(lineCount + "_" + lineContentRandom + ":_in line n_:_" + lineNumber + "_iteration_n:_" + i);

                } while (-1 != lineCount);

                lineNumberReader.setLineNumber(0);
                randomAccessFile.getChannel().position();
                positionFile = randomAccessFile.getFilePointer();

                //number of line in first dimention
                int n=i+1;

                //max of dimention of multidimentional array checked
                //ARRAY = new char[n][sizeLine][3];
                dimentionsArrayByFiles = new HashMap<String, List<Integer>>();

                ArrayList<Integer> listOfDimention =new ArrayList<Integer>();
                listOfDimention.add(n); //records Get(0)
                listOfDimention.add(sizeLine); //fields Get(1)

                threeDimention = (filePath == filePathCarte) ? 0 : (filePath == filePathRoader) ? 1 : null;

                 //three-dimention in ARRAY
                listOfDimention.add(threeDimention); //Get(2)
                listOfDimention.add(3); // Get(3) the dimentions three initialize ARRAY in three dimentional
                dimentionsArrayByFiles.put(filePath, listOfDimention);

                //return of starting position of cursor
                randomAccessFile.seek(0);


            } catch (IOException ex) {
                System.out.println(ex);
            } finally {
                System.out.println("ok subroutines Reading");
            }


            //return ARRAY;
            return dimentionsArrayByFiles;
        }

        public void subReadingInput(HashMap<String, List<Integer>> dimentionsArrayByFiles, char ARRAY[][][],
                                    String filePath) {
            try {

                RandomAccessFile randomAccessFile = new RandomAccessFile(filePath, "rw");

                int lineCount=0;
                int i = 0;
                int j = 0;
                int k = 0;
                int sizeLine;
                String lineContentRandom;
                do {
                    j = 0;
                    k = 0;

                    lineContentRandom = randomAccessFile.readLine();
                    sizeLine = lineContentRandom.getBytes().length;

                    do {

                        if ((char) lineContentRandom.charAt(k) != ',') {
                            //construction of ARRAY for mathematical calcule
                            ARRAY[i][j][dimentionsArrayByFiles.get(filePath).get(2)] = (char) lineContentRandom.charAt(k);
                            j = j + 1;
                        } else if ((char) lineContentRandom.charAt(k) == ',') {
                            //nothing
                        }
                        k = k + 1;
                    } while (k < sizeLine);

                    i = i + 1;

                } while (i < dimentionsArrayByFiles.get(filePath).get(0));


            } catch (IOException ex) {
                System.out.println(ex);
            } finally {
                System.out.println("ok subroutines SubReadingInput");
            }
        }
    }
}
