package PrincipalPack;

public class CoordonneesInput implements CoordonneesI {

    int valueX;
    int valueY;
    //by default
    public CoordonneesInput (){
        this.valueX=0;
        this.valueY=0;
    }

    CoordonneesInput (char initValueX, char initValueY){
        valueX=Character.getNumericValue(initValueX);
        valueY=Character.getNumericValue(initValueY);
    }
    public int getValueX() {
        return valueX;
    }

    public void setValueX(int valueX) {
        this.valueX = valueX;
    }

    public int getValueY() {
        return valueY;
    }

    public void setValueY(int valueY) {
        this.valueY = valueY;
    }


    public Orientation orientation = new Orientation();


    //une methode que move the cursor in function
    //du input du file
    //input position starting, array tridimentional, input mouvement OENS

    @Override
    public Orientation lectureCalculeNumerique(char[][][] ARRAY) {
        try {
            int j = 0;
            int newValueX = valueX;
            int newValueY = valueY;
            do {

                //System.out.println("before iteration newValueX_:_" + newValueX + "\n_________________newValueY_:_" + newValueY);
                if (dynamicMove(ARRAY, j, newValueX, newValueY).getVerticalHorizon() == 'h') {
                    //case axe X
                    newValueX = dynamicMove(ARRAY, j, newValueX, newValueY).getValueScalar();
                } else {
                    //case axe Y
                    newValueY = dynamicMove(ARRAY, j, newValueX, newValueY).getValueScalar();
                }

                j = j + 1;
            } while (ARRAY[1][j][1] != '\u0000');

            orientation.setFinalValueX(newValueX);
            orientation.setFinalValueY(newValueY);


        }catch (Exception e){
            System.out.print("Exception in subroutine CoordonnesInput"+e);
        }finally {
            System.out.println("Subroutine CoordonneesInput ok");
        }
        return orientation;
    }



    @Override
    public Orientation dynamicMove(char ARRAY[][][],int j,int valueX,int valueY) {

        switch(ARRAY[1][j][1]){

            case 'O':
                //stats
                valueX=valueX-1;
                orientation.valueScalar=valueX;
                orientation.verticalHorizon='h';
                return orientation;
            case 'E':
                //stats
                valueX=valueX+1;
                orientation.valueScalar=valueX;
                orientation.verticalHorizon='h';
                return orientation;
            case 'N':
                //stats
                valueY=valueY-1;
                orientation.valueScalar=valueY;
                orientation.verticalHorizon='v';
                return orientation;
            case 'S':
                //stats
                valueY=valueY+1;
                orientation.valueScalar=valueY;
                orientation.verticalHorizon='v';
                return orientation;
            default:
                //stats
                return orientation;

        }
    }
}
