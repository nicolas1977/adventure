package PrincipalPack;

public interface CoordonneesI {
    public Orientation lectureCalculeNumerique(char ARRAY[][][]);
    public Orientation dynamicMove(char ARRAY[][][], int j, int valueX,int valueY);
}
